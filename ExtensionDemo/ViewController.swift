//
//  ViewController.swift
//  ExtensionDemo
//
//  Created by James Cash on 18-11-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import ValueCommon

/* To add an extension:
  1. File > New > Target ... & pick the type of iOS extension you want
  2. Almost always add the "App Groups" Capabalitiy to the new target (under project, select the target, click "Capabilities", scroll to app groups, turn it on, create the group if needed, select the group)
  3. Add your common framework to the "linked frameworks & libraries"

Making a common framework is just File > New > Target, Cocoa Touch Framework (under iOS Framework & Library)
    When making a framework, make sure the things you want visible to consumers are "public" in swift
    Then you can just "import CommonStuff" framework in all your extensions
*/

class ViewController: UIViewController {

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueSlider: UISlider!

    var theValue: Float = 0.0

    func saveValue() {
        NSUserDefaults(suiteName: suiteName)?.setFloat(theValue, forKey: valueKey)
    }

    @IBAction func valueChanged(sender: UISlider) {
        theValue = sender.value
        setValueDisplay()
        saveValue()
    }

    func setValueDisplay() {
        valueSlider.value = theValue
        valueLabel.text = "\(theValue)"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let savedVal = NSUserDefaults(suiteName: suiteName)?.floatForKey(valueKey) {
            theValue = savedVal
        }
        setValueDisplay()
    }

    func refreshValue() {
        if let savedVal = NSUserDefaults(suiteName: suiteName)?.floatForKey(valueKey) {
            theValue = savedVal
        }
        setValueDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

