//
//  ShareViewController.swift
//  ValueShare
//
//  Created by James Cash on 18-11-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import ValueCommon

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        let numFormatter = NSNumberFormatter()
        numFormatter.allowsFloats = true
        let numValue = numFormatter.numberFromString(contentText)
        guard let num = numValue else { return false }
        return num.floatValue >= 0 && num.floatValue <= 1
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
        let numFormatter = NSNumberFormatter()
        numFormatter.allowsFloats = true
        guard let num = numFormatter.numberFromString(contentText) else { fatalError() }
        NSUserDefaults(suiteName: suiteName)?.setFloat(num.floatValue, forKey: valueKey)

        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        self.extensionContext!.completeRequestReturningItems([], completionHandler: nil)
    }

    override func configurationItems() -> [AnyObject]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
