//
//  TodayViewController.swift
//  ValueToday
//
//  Created by James Cash on 18-11-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import ValueCommon

class TodayViewController: UIViewController, NCWidgetProviding {

    var theValue: Float = 0.0

    @IBOutlet weak var valueSlider: UISlider!
        
    @IBAction func valueChanged(sender: UISlider) {
        theValue = sender.value
        NSUserDefaults(suiteName: suiteName)?.setFloat(theValue, forKey: valueKey)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let saved = NSUserDefaults(suiteName: suiteName)?.floatForKey(valueKey) {
            theValue = saved
        }
        valueSlider.value = theValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
}
