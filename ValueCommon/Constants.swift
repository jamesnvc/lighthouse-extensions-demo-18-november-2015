//
//  Constants.swift
//  ExtensionDemo
//
//  Created by James Cash on 18-11-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import Foundation

public let suiteName = "group.occasionallycogent.testing"
public let valueKey = "demoValueKey"